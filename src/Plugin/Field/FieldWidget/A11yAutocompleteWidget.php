<?php

declare(strict_types=1);

namespace Drupal\a11y_autocomplete_element\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a widget for a11y_automcomplete element.
 *
 * @FieldWidget(
  *   id = "a11y_autocomplete",
  *   label = @Translation("Accessible autocomplete"),
  *    field_types = {
 *     "entity_reference",
 *     "list_integer",
 *     "list_float",
 *     "list_string"
 *   },
 *   multiple_values = TRUE
  * )
 */
final class A11yAutocompleteWidget extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $formElement = parent::formElement($items, $delta, $element, $form, $form_state);
    $formElement['#type'] = 'a11y_autocomplete';
    return $formElement;
  }

}
