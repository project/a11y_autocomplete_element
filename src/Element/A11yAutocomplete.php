<?php

declare(strict_types=1);

namespace Drupal\a11y_autocomplete_element\Element;

use Drupal\Core\Render\Element\Select;

/**
 * Defines an element plugin for the Accessible Autocomplete library.
 *
 * Use this anywhere you'd use #type => select. All the keys from #select are
 * supported.
 *
 * @FormElement("a11y_autocomplete")
 */
final class A11yAutocomplete extends Select {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    $info = parent::getInfo();
    $class = static::class;
    $info['#process'][] = [$class, 'processA11yAutocomplete'];
    return $info;
  }

  /**
   * Process callback.
   */
  public static function processA11yAutocomplete(array $element): array {
    $element['#attached']['library'] = 'a11y_autocomplete_element/a11y_autocomplete_element';
    $element['#attributes']['data-a11y-autocomplete-element'] = TRUE;
    return $element;
  }

}
