/**
 * @file
 * Initiates a11y_autocomplete.
 */

import A11yAutocomplete from '@drupal/autocomplete';

((Drupal, once) => {
  /**
   * Initialize autocomplete
   *
   * @param {HTMLSelectElement} el Select element.
   */
  const init = el => {
    const setRequired = pillsContainer => {
      input.required =
        el.required &&
        !pillsContainer.querySelectorAll('.a11y-autocomplete__selected-item')
          .length;
    };
    const generatePillForOption = (pillsContainer, option) => {
      if (pillsContainer.querySelector(`[data-option="${option.value}"]`)) {
        return;
      }
      const pill = document.createElement('div');
      pill.classList.add('a11y-autocomplete__selected-item');
      pill.dataset.option = option.value;
      const close = document.createElement('button');
      close.classList.add('a11y-autocomplete__remove-selected');
      close.textContent = `Remove ${option.textContent}`;
      pill.textContent = option.textContent;
      close.addEventListener('click', () => {
        option.selected = false;
        pill.remove();
        setRequired(pillsContainer);
      });
      pill.append(close);
      pillsContainer.append(pill);
    };
    const input = document.createElement('input');
    input.type = 'text';
    input.classList.add('form-text', 'form-element', 'form-element--type-text');
    const parent = el.parentNode;
    el.classList.add('visually-hidden');
    parent.insertBefore(input, el);
    if (el.multiple === true) {
      const pills = document.createElement('div');
      pills.classList.add('a11y-autocomplete__selected-items');
      parent.insertBefore(pills, el);
      el.querySelectorAll('option[selected]').forEach(option => {
        generatePillForOption(pills, option);
      });
      setRequired(pills);
    } else {
      input.value =
        el.querySelector(`option[value="${el.value}"]`).textContent || '';
    }
    const items = Array.from(el.querySelectorAll('option')).reduce(
      (carry, option) => {
        carry.push({
          label: option.textContent,
          value: option.value,
        });
        return carry;
      },
      [],
    );
    // eslint-disable-next-line no-undef
    A11yAutocomplete(input, {
      source: items,
    });
    let selectionMade = false;
    input.addEventListener('autocomplete-select', e => {
      selectionMade = true;
      if (el.multiple === true) {
        const option = el.querySelector(
          `option[value="${e.detail.selected.value}"]`,
        );
        option.selected = true;
        generatePillForOption(
          parent.querySelector('.a11y-autocomplete__selected-items'),
          option,
        );
        return;
      }
      el.value = e.detail.selected.value;
      input.value = e.detail.selected.label;
    });
    // When we close the autocomplete element, set the value to an empty string if it's a multi-value item. If it's a
    // single value field, set it to the selected item label.
    input.addEventListener('autocomplete-close', () => {
      if (!selectionMade) {
        return;
      }
      selectionMade = false;
      if (el.multiple === true) {
        input.value = '';
        input.required = false;
        return;
      }
      input.value =
        el.querySelector(`option[value="${el.value}"]`).textContent || '';
    });
  };
  Drupal.behaviors.a11y_autocomplete_element = {
    attach(context) {
      once(
        'a11y_autocomplete_element',
        '[data-a11y-autocomplete-element]',
        context,
      ).forEach(init);
    },
  };
  // eslint-disable-next-line no-undef
})(Drupal, once);
